
/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SimplePostTest {

	static Logger logger = Logger.getLogger(SimplePostTest.class);

	@Test
	public void GetWeatherDetails() {
		RestAssured.baseURI = "https://reqres.in/api";

		RequestSpecification httpRequest = RestAssured.given();

		JSONObject requestParams = new JSONObject();
		requestParams.put("name", "morpheus"); // Cast
		requestParams.put("job", "leader");

		httpRequest.body(requestParams.toJSONString());
		Response response = httpRequest.get("/users");

		// Now let us print the body of the message to see what response
		// we have recieved from the server
		String responseBody = response.getBody().asString();

		logger.debug("------------------------------------------------------");
		logger.debug("response.getStatusCode() => " + response.getStatusCode());
		logger.debug("response.getStatusLine() => " + response.getStatusLine());
		logger.debug("Response Body is =>  " + responseBody);

	}

}
